<?php

/**
 * Maps each supported output format the their corresponding formatter and cli arguments
 */
return [
    'formatters' => [
        'csv' => [ExifGpsReader\Formatters\CsvFormatter::class, 'delim'],
        'html' => [ExifGpsReader\Formatters\TwigFormatter::class, 'template'],
    ],
];
