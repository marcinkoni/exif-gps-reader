<?php

declare(strict_types=1);

use ExifGpsReader\Formatters\FormatterFactory;
use ExifGpsReader\Formatters\CsvFormatter;
use ExifGpsReader\Formatters\TwigFormatter;
use Symfony\Component\Console\Input\InputInterface;
use PHPUnit\Framework\TestCase;

class FormatterFactoryTest extends TestCase
{
    public $headers = [1, 2, 3];
    public $testConfig = [
        'csv' => [CsvFormatter::class, 'delim'],
        'html' => [TwigFormatter::class, 'template'],
    ];

    public function setUp(): void
    {
        $this->renderFactory = new FormatterFactory($this->testConfig);
    }

    public function test_it_creates_correct_classes()
    {
        $mockInput = $this->getMockBuilder(InputInterface::class)->getMock();
        $mockInput->expects($this->exactly(2))->method('getOption')->withConsecutive(['delim'], ['template']);
        $this->assertInstanceOf(CsvFormatter::class, $this->renderFactory->makeFromInput('csv', $this->headers, $mockInput));
        $this->assertInstanceOf(TwigFormatter::class, $this->renderFactory->makeFromInput('html', $this->headers, $mockInput));
    }
}
