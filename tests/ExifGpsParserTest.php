<?php

declare(strict_types=1);

use ExifGpsReader\Parsers\ExifGpsParser;
use PHPUnit\Framework\TestCase;

class ExifGpsParserTest extends TestCase
{
    public $fileWithGps = __DIR__ . '/images/image_a.jpg';
    public $fileWithoutGps = __DIR__ . '/images/image_b.jpg';
    public $invalidFile = __DIR__ . '/images/not_an_image.jpg';

    public function setup(): void
    {
        $this->parser = new ExifGpsParser();
    }

    public function test_gets_correct_gps_values()
    {
        $this->assertEquals(
            [$this->fileWithGps, 50.0913333333333, -122.945666666667],
            $this->parser->parse($this->fileWithGps)
        );
        $this->assertEquals(
            [$this->fileWithoutGps, null, null],
            $this->parser->parse($this->fileWithoutGps)
        );
        $this->assertEquals(
            null,
            $this->parser->parse($this->invalidFile)
        );
    }
}
