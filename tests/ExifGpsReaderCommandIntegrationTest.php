<?php

declare(strict_types=1);

use ExifGpsReader\Commands\ExifGpsReaderCommand;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use PHPUnit\Framework\TestCase;

class ExifGpsReaderCommandIntegrationTest extends TestCase
{
    public $configPath = __DIR__ . '/../config/config.php';

    public function setUp(): void
    {
        $application = new Application();
        $application->add(new ExifGpsReaderCommand(require $this->configPath));
        $command = $application->find('exif-gps-reader');
        $this->commandTester = new CommandTester($command);
    }

    public function test_show_files_as_csv()
    {
        $this->commandTester->execute([
            'directory' => './tests/images/',
            '--recursive' => true,
            '--format' => 'csv',
            '--delim' => ',,'
        ]);
        $output = $this->commandTester->getDisplay();
        $this->assertEquals(
            ('"File",,"Latitude",,"Longitude"' . PHP_EOL .
                '"./tests/images/cats/image_e.jpg",,"59.924755079983",,"10.695598120067"' . PHP_EOL .
                '"./tests/images/image_a.jpg",,"50.091333333333",,"-122.94566666667"' . PHP_EOL .
                '"./tests/images/image_b.jpg",,"",,""' . PHP_EOL .
                '"./tests/images/image_c.jpg",,"38.4",,"-122.82866666667"' . PHP_EOL .
                '"./tests/images/image_d.jpg",,"",,""' . PHP_EOL),
            $output
        );
    }

    public function test_show_files_as_html()
    {
        $this->commandTester->execute([
            'directory' => './tests/images/',
            '--format' => 'html',
            '--template' => './src/Formatters/TwigFormatter/default-template.twig',
        ]);
        $output = $this->commandTester->getDisplay();
        $expected = file_get_contents('./tests/support/expected-default-output.html');
        $this->assertEqualXMLStructure($this->getBodyElement($expected), $this->getBodyElement($output));
    }

    public function getBodyElement($htmlStr)
    {
        $doc = new DOMDocument();
        $doc->loadHTML($htmlStr);
        return $doc->getElementsByTagName('body')->item(0);
    }
}
