<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use ExifGpsReader\Formatters\CsvFormatter;

class CsvFormatterTest extends TestCase
{
    public function test_renders_correctly()
    {
        $this->assertFormatterOutputs(
            ['Header1', 'Header2'],
            [['1', '', 3], ['3', false], [null, 2]],
            ['"Header1","Header2"' . PHP_EOL, '"1",""' . PHP_EOL, '"3",""' . PHP_EOL, '"","2"' . PHP_EOL]
        );
        $this->assertFormatterOutputs(
            [],
            [range(0, 10)],
            ['' . PHP_EOL, '' . PHP_EOL]
        );
    }

    public function test_escapes_quotes()
    {
        $this->assertFormatterOutputs(
            ['"Header"', '"""\"'],
            [['"', 'Start"End']],
            ['"""Header""","""""""\"""' . PHP_EOL, '"""","Start""End"' . PHP_EOL]
        );
    }

    public function assertFormatterOutputs($headers, $data, $expected)
    {
        $formatter = new CsvFormatter($headers);
        $output = iterator_to_array($formatter->format($data));
        $this->assertEquals($expected, $output);
    }
}
