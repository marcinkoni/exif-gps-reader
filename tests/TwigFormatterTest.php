<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use ExifGpsReader\Formatters\TwigFormatter;

class TwigFormatterTest extends TestCase
{
    public $templatePath = __DIR__ . '/support/test-template.twig';
    public $headers = ['H1', 'H2'];

    public function test_renders_correctly()
    {
        $data = [[1, 2], [null, 3, 4], []];
        $formatter = new TwigFormatter($this->headers, $this->templatePath);
        $output = iterator_to_array($formatter->format($data));
        $expected = [
            "Headers: H1,H2,",
            "Row: 1,2,",
            "Row: ,3,",
            "Row: ",
            "Footer",
        ];
        $this->assertEquals($expected, $output);
    }
}
