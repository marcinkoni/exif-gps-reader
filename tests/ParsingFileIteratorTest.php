<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use ExifGpsReader\ParsingFileIterator;
use ExifGpsReader\Parsers\ParserInterface;

class ParsingFileIteratorTest extends TestCase
{
    public $path = __DIR__ . '/images';

    public function setUp(): void
    {
        $this->parser = new class implements ParserInterface
        {
            public function parse(string $path): ?array
            {
                return ['Parsed: ' . $path];
            }
            public function getFields(): array
            {
                return ['Title'];
            }
        };
    }

    public function test_iterates_recursively()
    {
        $this->assertIteratorParsedFiles(new ParsingFileIterator($this->path, $this->parser, true), [
            '/cats/image_e.jpg',
            '/image_a.jpg',
            '/image_b.jpg',
            '/image_c.jpg',
            '/image_d.jpg',
            '/not_an_image.jpg',
        ]);
    }

    public function test_iterates_shallowly()
    {
        $this->assertIteratorParsedFiles(new ParsingFileIterator($this->path, $this->parser, false), [
            '/image_a.jpg',
            '/image_b.jpg',
            '/image_c.jpg',
            '/image_d.jpg',
            '/not_an_image.jpg',
        ]);
    }

    public function assertIteratorParsedFiles($iterator, $expectedFiles)
    {
        $output = iterator_to_array($iterator);
        $this->assertEquals(array_map(function ($file) {
            return ['Parsed: ' . $this->path . $file];
        }, $expectedFiles), $output);
    }
}
