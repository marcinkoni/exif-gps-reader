<?php

declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

use ExifGpsReader\Commands\ExifGpsReaderCommand;
use ExifGpsReader\ConsoleApplication;

/**
 * Instantiate console application.
 */
$version = '1.0.0';
$application = new ConsoleApplication('Exif Gps Reader', $version);
$command = new ExifGpsReaderCommand(require __DIR__ . '/config/config.php');

/**
 * Register and execute main command.
 */
$application->add($command);
$application->setDefaultCommand($command->getName(), true);
$application->run();
