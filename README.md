# Exif GPS Reader

A command line tool for reading gps data from exif headers in images. Output is streamed into STDOUT and can be piped into a file or application of choice. Supports multiple output formats such CSV and HTML, and can be easily extended to add more.

## Installation

This utility requires php >= 7.2 and the php exif extension (enabled by default in most installations). If you are on Windows you will have to enable `php_mbstring.dll` and `php_exif.dll` in your php.ini config.

Download or clone this repository and install the dependencies using composer:

`cd exif-gps-reader`, `composer install`.

## Usage

Once installed you can see the instructions by running:

`php exif-gps-reader.php --help`

```
Usage:
  exif-gps-reader [options] [--] [<directory>]

Arguments:
  directory                  Target directory (defaults to current directory) [default: "."]

Options:
  -f, --format[=FORMAT]      Select output format: csv, html [default: "csv"]
  -r, --recursive            Recursively parse directories
      --template[=TEMPLATE]  Html format only: Template location
      --delim[=DELIM]        Csv format only: Csv delimiter character
  -h, --help                 Display this help message
  -V, --version              Display this application version
```

By default, exif-gps-reader will shallowly search the working directory and output all exif GPS data in csv format to STDOUT:

`php exif-gps-reader.php tests/images`

You can pipe this output into a file or other cli tools:

`php exif-gps-reader.php --format html tests/images > output.html`

You may use the `--recursive` flag to parse directories recursively.

When using the csv format you may customize the delimiter with the `--delim` option like so: `--delim="|"`.

When using the html output format, you may use the `--template` option to provide a custom template like so: `--template=path/to/template`. Html templates use the [Twig](https://twig.symfony.com/) templating language and support being streamed out in blocks. See the example template in `src/Formatters/TwigFormatter/default-template.twig` for more details.

## Design

Given the context for this coding challenge, I built the projects to the requirements that it had to be extensible and could be integrated into a larger codebase. I chose to use the excellent Symfony Console package, which is what fuels other popular php cli tools such as Composer and Laravel's Artisan. The output formatters and file parser are set up so it's easy to add additional output targets (e.g. Laravel Blade templates) and to parse arbitrary data from files (e.g. file permissions). To avoid a large memory footprint all the components are designed to stream their output which is eventually piped directly into STDOUT.

## Future Improvements

Current tests are built to cover the major features, however the project would benefit from higher unit test coverage and additional integration tests. A limitation of existing tests is that they assume unix file paths, so they could be expanded to be platform agnostic. Once development is complete the tools should also be released as standalone bundled phar file, which could be easily executed from the command line after being added to the user's \$PATH.

## Contributing

You can run tests with `vendor/bin/phpunit tests`.
Exif GPS Reader is written according the [PSR-0/1/2 standards](http://www.php-fig.org/).
