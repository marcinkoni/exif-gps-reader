<?php

declare(strict_types=1);

namespace ExifGpsReader;

use ExifGpsReader\Parsers\ParserInterface;

/**
 * A (optionally recursive) file system iterator that returns parsed file data.
 */
class ParsingFileIterator implements \IteratorAggregate
{
    /**
     * @param $path Directory to iterate over.
     * @param $parser The parser that will extract data from file.
     * @param $recursive Flag to recursively parse child directories of $path.
     */
    public function __construct(string $path, ParserInterface $parser, bool $recursive = false)
    {
        if (!is_dir($path)) {
            throw new \InvalidArgumentException("No such directory: $path");
        }
        if (!is_readable($path)) {
            throw new \InvalidArgumentException("Permission denied: $path");
        }

        $this->path = $path;
        $this->parser = $parser;
        $this->recursive = $recursive;
    }

    /**
     * Provides iterable implementation for class. Parses each valid file before yielding.
     */
    public function getIterator(): \Iterator
    {
        foreach ($this->makeDirectoryIterator() as $fileInfo) {
            if (!$fileInfo->isFile()) {
                continue;
            }
            $parsed = $this->parser->parse($fileInfo->getPathName());
            if ($parsed) {
                yield $parsed;
            }
        }
    }

    /**
     * Creates directory iterator depending on recursion settings.
     */
    public function makeDirectoryIterator(): \Iterator
    {
        return ($this->recursive)
            ? new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($this->path),
                \RecursiveIteratorIterator::LEAVES_ONLY,
                \RecursiveIteratorIterator::CATCH_GET_CHILD
            )
            : new \DirectoryIterator($this->path);
    }
}
