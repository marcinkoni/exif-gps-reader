<?php

declare(strict_types=1);

namespace ExifGpsReader\Parsers;

interface ParserInterface
{
    /**
     * Reads file and returns an array of parsed file data.
     */
    public function parse(string $path): ?array;

    /**
     * Field names of the data that the parser reads.
     */
    public function getFields(): array;
}
