<?php

declare(strict_types=1);

namespace ExifGpsReader\Parsers;

use ExifGpsReader\Parsers\ParserInterface;

class ExifGpsParser implements ParserInterface
{
    const EXIF_LAT = 'GPSLatitude';
    const EXIF_LAT_REF = 'GPSLatitudeRef';
    const EXIF_LNG = 'GPSLongitude';
    const EXIF_LNG_REF = 'GPSLongitudeRef';

    /**
     * Field names of the parsed file data.
     */
    protected $fields = ['File', 'Latitude', 'Longitude'];

    /**
     * Extracts gps coordinates from exif headers and returns an array of fields.
     * If exif data exists but contains no coordinates, coordinates fields in array will be null.
     * If file cannot be read or does not contain exif data, returns null.
     *
     * @param string $path
     * @return array|null
     */
    public function parse(string $path): ?array
    {
        if (!$this->handlesFile($path)) {
            return null;
        }
        $gps = $this->getExifGps($path);
        return array_merge([$path], $gps);
    }

    /**
     * Determines if file contains exif header data.
     */
    protected function handlesFile(string $path): bool
    {
        return (bool) @exif_imagetype($path);
    }

    /**
     * Extracts gps coordinates from file with exif header data.
     */
    protected function getExifGps(string $path): array
    {
        $exif = exif_read_data($path);
        foreach ([self::EXIF_LAT, self::EXIF_LAT_REF, self::EXIF_LNG, self::EXIF_LNG_REF] as $key) {
            if (!isset($exif[$key])) {
                return [null, null];
            }
        }
        return [
            $this->decodeGpsCoord($exif[self::EXIF_LAT], $exif[self::EXIF_LAT_REF]),
            $this->decodeGpsCoord($exif[self::EXIF_LNG], $exif[self::EXIF_LNG_REF]),
        ];
    }

    /**
     * Exif GPS data is stored in an encoded format as Degrees/Minutes/Seconds (with hemisphere).
     * Decodes the format into a float coordinate.
     */
    protected function decodeGpsCoord(array $encoded, string $hemisphere): float
    {
        $degrees = $this->decodeGpsFloat($encoded[0]);
        $minutes = $this->decodeGpsFloat($encoded[1]);
        $seconds = $this->decodeGpsFloat($encoded[2]);
        $invert = ($hemisphere === 'S' || $hemisphere === 'W') ? -1 : 1;
        return $invert * ($degrees + $minutes / 60 + $seconds / 3600);
    }

    /**
     * Exif GPS numbers can be stored as a string fraction (e.g. "100/3").
     * Decodes GPS string into float.
     */
    protected function decodeGpsFloat(string $val): float
    {
        $parts = $val ? explode('/', $val) : null;
        if (!$parts) {
            return 0;
        }
        if (count($parts) === 1) {
            return $parts[0];
        }
        return (float) $parts[0] / (float) $parts[1];
    }

    /**
     * Returns field names of the parsed file data.
     */
    public function getFields(): array
    {
        return $this->fields;
    }
}
