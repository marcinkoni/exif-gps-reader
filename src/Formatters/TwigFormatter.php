<?php

declare(strict_types=1);

namespace ExifGpsReader\Formatters;

use ExifGpsReader\Formatters\Formatter;
use \Twig\Loader\FilesystemLoader;
use Twig\Environment;

class TwigFormatter extends Formatter
{
    protected $defaultTemplate = __DIR__ . '/TwigFormatter/default-template.twig';

    /**
     * @param $headers Data header names for formatted output.
     * @param $template Optional twig template file to render html from.
     */
    public function __construct(array $headers, ?string $template = null)
    {
        $this->headers = $headers;
        $this->template = $template ?: $this->defaultTemplate;
        $loader = new \Twig\Loader\FilesystemLoader(dirname($this->template));
        $this->twig = new Environment($loader, ['cache' => false]);
    }

    /**
     * Formats tabular data into html document generator.
     * @return iterable<string>
     */
    public function format(iterable $rows): iterable
    {
        $headers = $this->headers;
        $template = $this->twig->load(basename($this->template));
        yield $template->renderBlock('header', ['headers' => $headers]);
        foreach ($rows as $row) {
            yield $template->renderBlock(
                'row',
                ['headers' => $headers, 'row' => array_slice($row, 0, count($this->headers))]
            );
        }
        yield $template->renderBlock('footer', ['headers' => $headers]);
    }
}
