<?php

declare(strict_types=1);

namespace ExifGpsReader\Formatters;

use ExifGpsReader\Formatters\Formatter;
use Symfony\Component\Console\Input\InputInterface;

class FormatterFactory
{
    /**
     * @param $formatterConfig Map of $format keys to corresponding Formatter classes.
     */
    public function __construct(array $formatterConfig)
    {
        $this->formatterConfig = $formatterConfig;
    }

    /**
     * Instantiates correct formatter from $format and cli input arguments.
     */
    public function makeFromInput(string $format, array $headers, InputInterface $input): Formatter
    {
        $definition = $this->formatterConfig[$format] ?? null;
        if ($definition) {
            $class = $definition[0];
            $options = array_map(function ($opt) use ($input) {
                return $input->getOption($opt);
            }, array_slice($definition, 1));
            return new $class($headers, ...$options);
        }
        throw new \InvalidArgumentException(sprintf('Unsupported output format: %s', $format));
    }
}
