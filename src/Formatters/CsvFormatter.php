<?php

declare(strict_types=1);

namespace ExifGpsReader\Formatters;

use ExifGpsReader\Formatters\Formatter;

class CsvFormatter extends Formatter
{
    protected $lineDelimiter = PHP_EOL;

    public function __construct(array $headers, string $delimiter = null)
    {
        $this->headers = $headers;
        $this->delimiter = $delimiter ?: ',';
    }

    /**
     * Formats tabular data into Csv document iterator.
     * @return iterable<string>
     */
    public function format(iterable $rows): iterable
    {
        yield $this->formatRow($this->headers);
        foreach ($rows as $row) {
            yield $this->formatRow($row);
        }
    }

    protected function formatRow(array $row): string
    {
        $row = array_map(function ($x) {
            return '"' . str_replace('"', '""', $x) . '"';
        }, $row);
        $headers_count = count($this->headers);
        $row = count($row) > $headers_count ? array_slice($row, 0, $headers_count) : $row;
        return join($this->delimiter, $row) . $this->lineDelimiter;
    }
}
