<?php

declare(strict_types=1);

namespace ExifGpsReader\Formatters;

/**
 * Formats tabular/map data into desired output.
 */
abstract class Formatter
{
    /**
     * @var array Data header names for formatted output.
     */
    public function __construct(array $headers)
    {
        $this->headers = $headers;
    }

    /**
     * Formats tabular data into a desired output generator.
     * @param iterable<array> $rows Iterable rows containing array data.
     * @return iterable<string> Formatted output iterator.
     */
    abstract public function format(iterable $rows): iterable;
}
