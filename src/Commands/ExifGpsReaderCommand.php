<?php

declare(strict_types=1);

namespace ExifGpsReader\Commands;

use ExifGpsReader\ParsingFileIterator;
use ExifGpsReader\Formatters\FormatterFactory;
use ExifGpsReader\Parsers\ExifGpsParser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExifGpsReaderCommand extends Command
{
    /**
     * @param $config Command configuration. Contains $format => Formatter class array.
     */
    public function __construct(array $config)
    {
        $this->config = $config;
        parent::__construct();
    }

    /**
     * Configures command with name, arguments and options.
     */
    protected function configure(): void
    {
        $supportedFormats = join(', ', array_keys($this->config['formatters']));
        $this->setName('exif-gps-reader')
            ->addArgument('directory', InputArgument::OPTIONAL, 'Target directory (defaults to current directory)', '.')
            ->addOption('format', 'f', InputOption::VALUE_OPTIONAL, "Select output format: $supportedFormats", 'csv')
            ->addOption('recursive', 'r', InputOption::VALUE_NONE, 'Recursively parse directories')
            ->addOption('template', '', InputOption::VALUE_OPTIONAL, 'Html format only: Template location')
            ->addOption('delim', '', InputOption::VALUE_OPTIONAL, 'Csv format only: Csv delimiter character');
    }

    /**
     * Executes main command functionality depending of provided arguments.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $format = $input->getOption('format');
        $directory = $input->getArgument('directory');
        $recursive = $input->hasParameterOption('--recursive') || $input->hasParameterOption('-r');

        $parser = new ExifGpsParser();
        try {
            $iterator = new ParsingFileIterator($directory, $parser, $recursive);
            $formatter = (new FormatterFactory($this->config['formatters']))
                ->makeFromInput($format, $parser->getFields(), $input);
            foreach ($formatter->format($iterator) as $chunk) {
                $output->write($chunk);
            }
        } catch (\InvalidArgumentException $e) {
            return $this->error($output, $e->getMessage());
        }
        return 0;
    }

    /**
     * Return with error code after rendering a message to output.
     */
    protected function error(OutputInterface $output, string $message): int
    {
        $output->writeln('<fg=red>' . $message . '</>');
        return 1;
    }
}
